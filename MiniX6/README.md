#### ReadMe - MiniX6 - Object abstraction

RunMe: https://marieak.gitlab.io/aestheticprogramming/MiniX6/

Code: https://gitlab.com/marieak/aestheticprogramming/-/blob/main/MiniX6/sketch.js


DISCLAIMER: The fish does not show up in my game at the moment. Currently working on it!!

### How does my game work?
This games code is sampled from Winnies tofu-game. I found it really difficult to come up execute my own idea. Even though I had plenty of ideas, I simply do not have the skills to go thorugh with them. I therefore decided to use Winnies code, and change a couple of bits.
I wanted to make a arctic theme, with a polarbear in the background, and the main "player" being a penguin that has to catch fish - just like pacman eats the tofu in Winnies game.
The goal of the game is to eat as many fish as possible, and not waste/lose too many. If too many fish are lost, the game ends. Lowest on the screen the score of how many fish has been catched can be seen. You move the penguin up and down with the up and down arrows of the keyboard.

### How the programmed objects work
For this MiniX I made the class "Fish" which are the fish flying in from the right side, that the penguin then has to catch. It works the same way as Winnie's tofu code. The function constructor() initializes the object with the attributes below, which involves variables such as properties of speed, position, size, rotating etc. Because my class is a picture of a fish, it might work a little different. I have had many problems with the fish even showing up... . To display the fish on the screen I had to use the method show() since without it, the object will be created in the background and not show on the screen.

### The characteristics of object-oriented programming and the wider implications of abstraction
It is important to remember that “objects are designed with certain assumptions, biases, and worldviews (Soon & Cox, 2020)” so that we can prevent unethical abstractions and instead create or redefine objects in a way that represents us all. 
Some of the key characteristics of object-oriented programming is properties and behaviors. We spend a lot of time this week figuring out how "classes" work. With classes you are able to customize certain elements of the code in a much more manageable way. Creating classes and what you put in these classes applies throughout the entire code.

### A wider cultural context 
My intention with the chosen design of my game, is to make the player think about the life in the arctic. Lately i've watched a lot of documentaries on how global warming affects the life on the arctic - and it is very critical. Even though the game does not "do" or "change" anything, I just hope that the picture of the polarbear and the penguin might remind the player that global warming is happening right now.
On a more simple side, the game symbolizes the food chain, as the penguin is eating/catching the fish. The game is not created with a deeper cultural or social norm og political aspect, but the intention is more like a simple child game with a theme that everyone can understand.


Winnie Soon & Geoff Cox(2020) Aesthetic Programming: A Handbook of Software Studies. 

![](screenshot.png)

