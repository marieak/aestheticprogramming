/*based on ES6 (class-based object oriented programming is
introduced in ECMAScript 2015)
credit and inspiration:
game scene: ToFu Go by Francis Lam; emoji: Multi by David Reinfurt*/
let pacmanSize = {
  w:156,
  h:159
};
let pacman;
let pacPosY;
let mini_height;
let min_fish = 5;  //min fish on the screen
let fish = [];
let score =0, lose = 0;
let keyColor = 45;
let img

function preload(){
  pacman = loadImage("penguin.png");
  img = loadImage("arctic.jpeg");
  fish = loadImage("fish.png");
}

function setup() {
createCanvas(windowWidth, windowHeight);

pacPosY = height/2;
mini_height = height/2;


}
function draw() {
  background(0,0,255);
  image(img,0,0,1500,900);
  fill(keyColor, 255);
  rect(0, height/1.5, width, 1);
  displayScore();
  checkfishNum(); //available fish
  showfish();
  image(pacman, 0, pacPosY, pacmanSize.w, pacmanSize.h);
  checkEating(); //scoring
  checkResult();
}

function checkfishNum() {
  if (fish.length < min_fish) { //amount of fish
    fish.push(new fish()); //er antallet mindre end minimum (5) så lav en ny fisk
  }
}

function showfish(){
  for (let i = 0; i <fish.length; i++) {
    fish[i].move();
    fish[i].show();
  }
}

function checkEating() {
  //calculate the distance between each fish
  for (let i = 0; i < fish.length; i++) {
    let d = int(
      dist(pacmanSize.w/2, pacPosY+pacmanSize.h/2,
        fish[i].pos.x, fish[i].pos.y)
      );
    if (d < pacmanSize.w/2.5) { //close enough as if eating the fish
      score++;
      fish.splice(i,1);
    }else if (fish[i].pos.x < 3) { //penguin missed the fish
      lose++;
      fish.splice(i,1);
    }
  }
}

function displayScore() {
    fill(keyColor, 160);
    textSize(17);
    textStyle(BOLD)
    textFont("monospace");
    text('You have eaten '+ score + " fish", 10, height/1.4);
    text('You have wasted ' + lose + " fish", 10, height/1.4+20);
    fill(keyColor,255);
    text('PRESS the ARROW UP & DOWN key to eat the fish',
    10, height/1.4+40);
}

function checkResult() {
  if (lose > score && lose > 2) {
    fill(keyColor, 255);
    textStyle(BOLD)
    textFont("monospace");
    textSize(26);
    text("Too Much WASTAGE...GAME OVER", width/3, height/1.4);
    noLoop();
  }
}

function keyPressed() {
  if (keyCode === UP_ARROW) {
    pacPosY-=50;
  } else if (keyCode === DOWN_ARROW) {
    pacPosY+=50;
  }
  //reset if the penguin moves out of range
  if (pacPosY > mini_height) {
    pacPosY = mini_height;
  } else if (pacPosY < 0 - pacmanSize.w/2) {
    pacPosY = 0;
  }

  // Prevent default browser behaviour
  // attached to key events.
  return false;
}
