let earth //load the earth image
function preload(){
earth = loadImage('earth_1_.png');

}

let hearts = ['💛','💙'];

function setup(){
  push()
  createCanvas(windowWidth, windowHeight);
  background(241, 212, 5); //yellow
  fill(11, 75, 198); //blue
  noStroke();
  rect(0, 0, windowWidth, windowHeight/2); //rect that fills half of window
  frameRate(10); //set speed of the throbber

  pop(); //earth image in the center
  imageMode(CENTER)
  image(earth,720,395)

}

function draw(){ //the text
  fill(0,0,255)
   textSize(25)
   textStyle(BOLD)
   textFont("monospace");
   text("Waiting for peace on earth...",900,570)
    drawElements();
}
function drawElements() {
  let rand = floor(random(hearts.length));//picks a random heart from the array to draw in the circle
  let allHearts = hearts[rand];

  let num=15;// number of hearts in the circle
  translate(width/2, height/2);// moving the circle to the center
  let cir = 360/num*(frameCount%num); // how many degrees the circle is rotating by each frame.
  rotate(radians(cir));
  textSize(25);
  text(allHearts,80,80);

  }
