# ReadMe - MiniX3

RunMe: https://marieak.gitlab.io/aestheticprogramming/MiniX3/

Link to code: https://gitlab.com/marieak/aestheticprogramming/-/blob/main/MiniX3/sketch.js

### What I created - what I want to explore/express
I have made a throbber consistent of the emoji's "💙💛", that goes around in a circle, on the background of the Ukranian flag. I also added the text "Waiting for peace on earth..."
For this weeks miniX, I knew right away that I wanted to make something that has to do with the invasion of Ukraine that began this week. I find the situation really terrible, and it is clear how it affects a lot of people - even here in Denmark. My goal with this miniX is to make the viewer reflect about the politcal chaos that is going on - and hopefully agree with me - that we all just want peace on earth.
A throbber to me, is something that is wasting your time. Nothing else is happening, it's just going around and around. That is a similar to the war happening right now. What is the purpose? I find this war so unnecessary. It is wasting time, money, and LIVES 🥺.

### What are the time related syntax/functions used in this program?
I found it quite difficult to make the throbber! It took me a while to figure out how the syntax works... After a while, I figured out that to make the hearts go around in a circle, I needed this: let cir = 360/num*(frameCount%num); 
The rotate syntax also plays a role in my code, as it makes the hearts go round in a circle.

I knew from last week how to add an image to the sketch, so that part did not cost me much trouble. 
The most challenging part was defining my "own" function, to make the hearts go round. Here I used: function drawElements() {

### How might we characterize the icon of a throbber differently?
A throbber to me mostly communicates waiting time. It's going round and round, most often because something is loading. So throbbers to me are connected to a certain amount of frustration, as all I do is  sit and watch it spin. Could we instead of being frustrated and distracted by our screens all the time, maybe stop for just a second and breathe when we see a throbber? It should be possible 🌞






![](screenshottb.png)
