### ReadMe - MiniX7 - Revisit the past


RunMe: https://marieak.gitlab.io/aestheticprogramming/MiniX7/

Code: https://gitlab.com/marieak/aestheticprogramming/-/blob/main/MiniX7/sketch.js


### Which MiniX have you reworked?
For this weeks miniX, I reworked my miniX2 which had the theme Variable Geometry/Geometric Emoji. I chose this one because I was sick with covid19 the week we made this miniX. I was not satisfied with my work, so this week I wanted to give it a makeover. When I made my minix2 I created a girl with acne, and a little yellow sun in the corner. There was no connection between the two, and all in all it was a mess... I had so many plans and ideas, but due to my sickness I did not manage to achieve my goal.


### What have you changed and why?
I actually chose the same "theme" for my emoji, with it being an emoji with skin problems/acne. I changed my emoji from being a white girl, to it being a yellow circle face. I thought about my miniX2 in a bigger perspective, and discovered that it's not very inclusive being a white girl. So I thought I would make the emoji more "universal" by chosing the "standard" emoji layout with a yellow circle. I also made it more interactive, as it is now possible to press the mouse, and the eyes are following the mouse around on the screen. I chose to change this to show how my skills have developed over the last weeks. I also made a picture as a background - and I made the background picture myself on an app on my IPhone. This gave me new opportunities for my miniX to look a little more complicated than it really is :)
In relation to my ReadMe, there will be more focus on the digital culture and social context in this one, compared to the miniX2 where I did not go into depth with the themes.


### What have you learnt in this miniX? 
I learned how to make an object follow the mouse around the screen using let x1 = map(mouseX, 0, width,250,280);  let y1 = map(mouseY, 0, height,290,320); I have never managed to get this syntax to work in the past, so that was a big win for me. I also learned how the mousePressed can be executed in different ways, depending on how and what you want your sketch to look like.
I also looked at the feedback I got from my classmates for my miniX2. They told me it was a great idea to create an emoji with acne, and they commented on how Apple already has some emojis with disabilities - so why not expand that. The girls also told me that they were curious to see it my sketch "did" anything - which unfortunately it did not. So I really wanted to make some more interactive aspects in the new miniX.


##What is the relation between aesthetic programming and digital culture? How does your work demonstrate the perspective of aesthetic programming?
There is an important relation between aesthetic programming and digital culture. When I first heard the words "aesthetic programming", I thought it was aesthetic in relation to it having to look very aesthetically pleasing. I soon discovered that was not the case. In this course, the aesthetic POV is focused on inclusion, avaliability and how to use programming to make a difference. The creators of p5.js makes a big deal out of wanting to make coding more available to people of all genders and races, and I believe this could and **should** be the standard "rules" of the digital culture.

With my emoji, I want to represent a group of people that I myself can relate to - but also create awarness to the fact that I really do believe acne is beautiful and not something you should be ashamed of or feel like you need to hide. The face is such a big deal of a persons identity, and it really is what makes us all unique from each other:

_"The face clearly occupies a central position in everyday life and social interaction, and it almost goes without saying that its features are perceived to display our uniqueness and individuality." (Soon & Cox, 2020)_

By creating an emoji with a skin disease, the goal is to make acne more normalized. Almost everyone has it in some point of their lives, and there is nothing wrong with it. We need to normalize it! Just like we need to normalize that females can be coders and programmers, as well as people of all races. This is where aesthetic programming can make a difference in the digital culture.


**Aesthetic Programming, Winnie Soon & Geoff Cox, 2020**

![](screen.png)
