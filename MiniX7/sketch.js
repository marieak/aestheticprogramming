// load the pictures used as background
let sad
let happy
function preload(){
sad = loadImage('sad.PNG');
happy = loadImage('happy.PNG');
}

// setting up canvas + background
function setup(){
createCanvas(700,700);
}

//The setup for when the mouse is pressed
function draw(){
background(50);
image(happy, 0,0,700,700);

if (mouseIsPressed === true) {

//Black outline of eyes
fill(0);
ellipse(260,300,80,120);
ellipse(420,300,80,120);

//left eye tracker
let x1 = map(mouseX, 0, width,250,280);
let y1 = map(mouseY, 0, height,290,320);

//left eye
fill(255);
ellipse(x1, y1, 40, 70);

//right eye tracker
let x2 = map(mouseX, 0, width,420,440);
let y2 = map(mouseY, 0, height,290,320);

//right eye
fill(255);
ellipse(x2, y2, 40, 70);

//yellow pimples
fill(255,255,255);
stroke(255,255,5);
strokeWeight(2);
ellipse(490,380,10,10);
ellipse(430,200,10,10);
ellipse(200,320,5,5);
ellipse(290,530,5,5);
ellipse(320,480,10,10);
ellipse(340,330,10,10);
ellipse(300,220,10,10);
ellipse(280,370,10,10);
ellipse(240,420,10,10);
ellipse(440,420,10,10);
ellipse(480,505,10,10);
ellipse(490,290,10,10);
//red pimples
fill(153,0,0);
noStroke();
ellipse(240,390,10,10);
ellipse(240,220,10,10);
ellipse(490,470,10,10);
ellipse(300,200,10,10);
//Text
fill(255);
textSize(30);
textFont('Helvetica');
textStyle(ITALIC);
text("Acne is beautiful ✨ ", 230,140);

} else {

//The setup before mouse is pressed
background(0);
image(sad, 0,0,700,700);

//black outline of the eyes
fill(0);
ellipse(260,300,80,120);
ellipse(420,300,80,120);
//Left eye tracker

let x1 = map(mouseX, 0, width,250,280);
let y1 = map(mouseY, 0, height,290,320);

//left eye
fill(255);
ellipse(x1, y1, 40, 70);

//right eye tracker
let x2 = map(mouseX, 0, width,420,440);
let y2 = map(mouseY, 0, height,290,320);

//right eye
fill(255);
ellipse(x2, y2, 40, 70);

//yellow pimples
fill(255,255,255);
stroke(255,255,5);
strokeWeight(2);
ellipse(490,380,10,10);
ellipse(430,200,10,10);
ellipse(200,320,5,5);
ellipse(290,530,5,5);
ellipse(320,480,10,10);
ellipse(340,330,10,10);
ellipse(300,220,10,10);
//red pimples
fill(153,0,0);
noStroke();
ellipse(240,390,10,10);
ellipse(240,220,10,10);
ellipse(490,470,10,10);

//text
fill(255);
textSize(22);
textFont('Helvetica');
textStyle(ITALIC);
text("Press the mouse to remove the pimples", 160,140);

print(mouseIsPressed);

}
}
