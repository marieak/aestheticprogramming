# ReadMe - MiniX2

RunMe: https://marieak.gitlab.io/aestheticprogramming/MiniX2/


Link to code: https://gitlab.com/marieak/aestheticprogramming/-/blob/main/MiniX2/sketch.js



### The story behind my Emojis 😎⭐️

For this weeks miniX, I spent a lot of time thinking about which emojis I wanted to make. My head was filled with ideas, and I quickly figured out that I wanted to make an emoji with acne/skin issues. This might not be a "political" problem, but I still think it makes sense to talk about it in relation to emoji's being universal. As I have struggled a lot with skin problems and acne myself, I find that there is a lot of shame connected to this condition. If there was an Emoji with "bad" skin, I think it would make it less embarrasing, and far more normal to talk about and discuss the insecurities that comes with acne. 

Other than that, I tried to create a sun with sunglasses. I wanted there to be "sunshine lines" coming out of the sun - but I couldn't figure out how to do that...

### The code
I wanted to challenge myself this week, by learning something new. I figured out how to add an image to the sketch, and make it follow the mouse around. To add the image, I used the function preload (); and then added the name of the picture. To make it follow the mouse around, I used pop();
imageMode(CENTER), image(creme, mouseX, mouseY);. Other than that, the code mostly consists of circles and rectangles, as these are the things I feel most confident in playing with, as I am still very new in this field. By doing this weeks code, I figured out how important it is to have the functions in the right order, so everything is placed where it is supposed to be.

I really wanted to create a function with mousePressed, but I could not get that to work. I'm not sure whether it is because of the imagine following the mouse around, but my original idea was that when the mouse is pressed, the acne would dissapear. Sadly, I could not get that to work either.. 🙃

### The context
The social and cultural context of my emoji is as mentioned earlier meant to represent this very common skin condition, and make it more normal - and less shamefull. I also wanted to make the skincolor change, to make it more universal. Sadly, this did not work out for me either (my lack of experience really challenged me this week), so instead I took inspiration from myself. Skincolor is definetly something that should be changed to make the emoji more accesible for more people, but as we have read in the texts, skincolor in emojis is a very complex theme.

### What went wrong
This week i got Covid.🦠🦠🦠 I have been very sick, and therefore I was not able to spend the amount of time on this Minix that I would have liked to. 
I spend a lot of time by myself, trying to figure out the different functions, but being in isolation with no one to ask, it can be kind of difficult. I am very sad I did not get to enter the Instructor or the Shut Up And Code this week - I really think that would have helped a lot. 🥸

![](emoji_screenshot.png)
