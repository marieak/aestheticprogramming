let creme

function preload(){
  creme = loadImage('creme1.png');
}
// Background
function setup(){
createCanvas(600,500);

}

function draw(){
noStroke();

  background(204,204,255);

//back of the hair - needs to be under everything else
fill(102,51,0)
ellipse(270,400,230,100);

//head and body
fill(51,51,255);
ellipse(270,480,300,120);
fill(255,204,153);
rectMode(CENTER);
rect(270,300,200,250,80);

//eyes
stroke(0,0,0);
fill(255,255,255);
ellipse(230,260,50,30);
fill(255,255,255);
ellipse(310,260,50,30);
fill(0,0,255);
ellipse(230,260,25,25);
fill(0,0,255);
ellipse(310, 260, 25, 25);
fill(0);
ellipse(310,260,15,15);
ellipse(230,260,15,15);

//mouth
noStroke();
fill(255,0,0);
ellipse(270,350,90,70);
fill(255,204,153);
rect(270,325,90,70);

//nose
stroke(0);
rect(270,320,10,20,100);

//pimples

fill(255,255,255);
stroke(255,255,0);
strokeWeight(2);
ellipse(220,380,10,10);
ellipse(240,330,10,10);
ellipse(210,320,5,5);
ellipse(290,330,5,5);
ellipse(320,380,10,10);
ellipse(340,330,10,10);
ellipse(300,220,10,10);
fill(153,0,0);
noStroke();
ellipse(240,390,5,5);
ellipse(240,220,5,5);

//hair
fill(102,51,0);
ellipse(270,190,230,40);
rect(170,430,40,500,30);
rect(370,430,40,500,30);

//sun
fill(250,219,65)
ellipse(90,90,150);
//sunglasses on sun
fill(0);
ellipse(60,70,40);
ellipse(120,70,40);
rect(90,70,150,10);
//sun mouth
stroke(255);
bezier(60, 130, 70, 160, 80, 140, 150, 100, 150, 120, 60, );

//creme following the mouse
pop();
imageMode(CENTER)
image(creme, mouseX, mouseY);
}
