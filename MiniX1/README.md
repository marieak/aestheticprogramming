#  MiniX1

RunMe: https://marieak.gitlab.io/aestheticprogramming/MiniX1/

Code: https://gitlab.com/marieak/aestheticprogramming/-/blob/main/MiniX1/sketch.js

### What have you produced?
My project: Hi pumpkin
For my first MiniX I focused on getting to know the different programs, as well as how to make simple shapes in ATOM. I played around a lot with different shapes, and already feel like I have got the hang of how to do both circles, triangles and squares.
It was interresting to me how much reasearch I had to do on my own, to figure out how to do different things. How do I change the colours? I found a website with a great colourscheme, and after a while I began to understand the RGB scheme.

My design is a blue "pumpkin" with eyes and a mouth that blinks. For the code of the blinking face features, I found some inspiration[Here](https://happycoding.io/examples/p5js/animation/flickering-jack-o-lantern). I added the text "HI PUMPKIN" because I thought it was cute, and I liked playing around with the text feature.
First, I created the canvas, and the first background color. I then wanted to play around and add some layers, so I made a couple more reactangles and placed them on top of the background. The I made the "pumpkin" itself, with the stem and everything. This was a little hard, as I wanted things to be in the middle of the canvas. Lastly I added the blinking face and text.

###  The first programming experience

I actually really enjoyed having to program and code for the first time ever. It was frustrating, but also very exiting everytime something new made sense. I now understand that playing around in the program, and using it for hours helps me get more comfortable, as some of the code begins to look familiar and make sense.
It was challenging! But also quite fun to dive into this whole new world for the first time. I found it almost crazy how exact the spelling has to be. If a dot . or a / is not placed correctly, everthing stops working! I know that's going to be a challenge in the feature....

### The difference from reading/writing text
When I write text, I use the Microsoft program Word. In this program, it helps you with spelling when something is wrong. That is a very big difference, as in ATOM it is up to yourself to find the mistake! But I also believe that programming-language is just a new language to learn. I'm already understanding more and more when looking at other peoples code. It's like learning to read for the first time again :)

### What does code and programming mean to me? + reflections from the readings
At the moment, code and programming does not mean a lot to me. I think i am still too "new" in this world, to fully understand that programming can be used to make a difference. But reading the chapter "Getting Started" by Winnie and Geoff, and watching the video by Lauren McCarthy made me realise that this could potentially be used for something bigger!


This was my first time EVER coding. I am very excited to learn though. Hopefully in the end I can turn in my MiniX's with a little more confidence than I do with this one :)


Soon Winnie & Cox, Geoff, "Getting started", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 27-48
Video: Lauren McCarthy, Learning While making P5.JS, OPENVIS Conference (2015).

![](Pumpkin_screenshot.png)
