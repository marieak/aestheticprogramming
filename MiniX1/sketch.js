// Background
function setup(){
  createCanvas(500,500);
  background(0,0,153);
  frameRate(10);

}

fill(51,153,255);
  rect(20, 15, 460, 470);

  fill(204,229,255);
    rect(45, 35, 415, 420);

// Stem of the fruit
function draw(){
  stroke(0,160,0);
  strokeWeight(20);
  line(220,150,255,50);

// The fruit
fill(0,0,255);
stroke(120, 60, 0);
strokeWeight(3);

ellipse(250, 250, 400, 300);

//Blinking eyes
var fireRed = random(255);
 var fireGreen = random(fireRed);
 var fireBlue = random(fireGreen);
 fill(fireRed, fireGreen, fireBlue);
 stroke(0);

//Eyes and mouth
 triangle(
    175, 200,
    150, 225,
    200, 225);
  triangle(
    325, 200,
    300, 225,
    350, 225);

    arc(
    250, 275,
    250, 75,
    radians(0), radians(180),
    CHORD);

    //text
    textSize(32);
text('HI PUMPKIN', 160, 435);
fill(0, 102, 153);

  }
