#### ReadMe - MiniX4 - Capture All

RunMe:https://marieak.gitlab.io/aestheticprogramming/MiniX4/


Code: https://gitlab.com/marieak/aestheticprogramming/-/blob/main/MiniX4/sketch.js


### My program called "Self centered 💎"
This week we talked and read about data capture. The theme this week called "Capture All" made my mind think of how much data is captured without us fully knowing what is going on. Most of the time, we just click the "yes" or "accept" button, and believe that it is all good. But does anyone really know what data the website/app is "taking" from us?

### How my program adress the theme "Capture All"
When the browser is first opened, a very "girly" site can be seen. In the middle is a little display of the users webcam. The text above the cam says "You really like looking at yourself dont you", which I just thought was funny and went well with the girly theme. When the button "yes I do" is pressed, a text saying "so do we, you are now being recorded" shows up. The idea behind this, is that we often just click buttons without giving much thought into what data we then are providing them with. I would get scared if I experienced this, and found out that my webcam was recording, and that someone then had my data/video.

### The Code
This week we learned how to use both webcam and sound in the code, as well as a face-tracker and Create Button. So the new syntax I used this week was the 
let capture;
capture = createCapture(VIDEO);
capture.size(800, 600);
capture.hide();
image(capture, 440, 200, 500, 500 * capture.height / capture.width);

Other than this, I learned how to create a button, and acctually make it DO something. Here I used the syntax:
button = createButton('yes I do 🦋');
button.position(630, 635);
button.size(120,50);
button.mousePressed(soDowe);

It was exciting to learn how to make the button work, and I kind of enjoyed it 🌞

Soon Winnie & Cox, Geoff, "Data capture", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 97-119

![](screenshot4.png)
