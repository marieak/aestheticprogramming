let capture;
let button;

function setup() {
//webcam capture
createCanvas(windowWidth,windowHeight);
background(221,160,221);
capture = createCapture(VIDEO);
capture.size(800, 600);
capture.hide();

//button
button = createButton('yes I do 🦋');
button.position(630, 635);
button.size(120,50);
button.mousePressed(soDowe);

// when button pressed
function soDowe() {
fill(0);
textSize(28);
textFont('Helvetica');
textStyle(ITALIC);
text('So do we, you are now being recorded', 470, 130);
}
}

//the squares with webcam inside
function draw() {
fill(255,153,204);
strokeWeight(3);
rect(410,150,560,560);
fill(238,130,238);
rect(430,190,520,400);
image(capture, 440, 200, 500, 500 * capture.height / capture.width);

//text
fill(255);
textSize(20);
textFont('Helvetica');
textStyle(ITALIC);
text("you really like looking at yourself dont you ✨",505,90);

//emojis
textSize(40);
text("💛🌼✨",205,100);
textSize(40);
text("💌💜💖",240,300);
textSize(30);
text("🦄🌸🌺🌹",100,600);
textSize(30);
text("⭐️🥺",1200,300);
textSize(30);
text("🌻🌞💫",1300,600);
textSize(30);
text("💎🔮🎀🛍",1200,100);
}
