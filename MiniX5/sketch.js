let bip = 40;
let emoji = ["🌞","🦁","🐠","🍀","🌈","🍌","🥐","🏀","🏆","🧩","🚗","⏰","🧽","📬","📀","💎"]

function setup() {
  createCanvas(windowWidth, windowHeight);
  background(206,246,255);
  frameRate(50);
}

function draw() {
  noStroke();
/*chooses a random number between 0 and 1.
if this number is bigger than 0.5, it will draw a square, in a random color, size and location.
if it, on the other hand, is smaller than 0.5, it will draw a random emoji (one of the preloaded) in random size and location.*/
if (random(1) < 0.5) {
  fill(random(255),random(255),random(255));
  square(random(width),(random(height)), random(20,50));
} else {
  textSize(random(10,50));
  text(emoji[floor(random(28))],random(width),random(height));
  }

//creates the starting square in a random color.
while (bip >= 20) {
/* subtracts 20 from bip, making the bip value equal 20.
shouldn't have any meaning in relation to the program itself, but it doesn't work without this line.*/
  bip = bip - 20;
  fill(random(255),random(255),random(255));
  square(200,200,30);


}
//used this to try and figure out why the 'while' loop did what it did. Still don't know why the line is necessary.
console.log(bip);
}
