#### ReadMe - MiniX5 

[RunMe](https://marieak.gitlab.io/aestheticprogramming/MiniX5/)

[Code](https://gitlab.com/marieak/aestheticprogramming/-/blob/main/MiniX5/sketch.js)


### The program
This week we talked a lot about randomness as well as how to create an auto-generated program. I find it almost hypnotising how the program just keeps going and going, and in fact never stops.
My program is quite simple. All that happens, is that random squares in random colors, as well as a range of random emojies are drawn on the screen.

### The rules in my miniX:
1 - When the random-syntax chooses a number higher than 0.5, it will draw a square in a random color, location, and size.


2 - When it chooses a number lower than 0.5 instead, it will draw a random emoji out of the ones I chose for the program.

Over time nothing new happens - it just keeps going. The program will not know when the canvas is full, and will therefor just keep creating squares and emojis forever.

### What role does the rules play
The rules in my program is what makes it look "random". The shift between emojis and squares makes this "messy" looking canvas which is what I was going for. I think it is quite fun how the computer is responsible for the outcome of the canvas, as it will look different everytime you refresh the page. 

### The Code
This week we learned how to create a program, that auto-generates. I found that quite hard, as it is not like any of the code I have tried or played around with before. There is a bug with the code which I need to fix, as some of the code is unessecary, but it does not work without it. :)

![](screen.png)
